package org.harouna.tpxml.serie1Exercice2;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;

public class MainExercice2 {

		public static void main(String[] args) throws DocumentException {
			// TODO Auto-generated method stub
			XMLUtil xml = new XMLUtil();
			
			File xmlFile = new File("files/marin.xml");
			Document d = xml.read(xmlFile);
			Marin marin = xml.deserialize(d);
			
			System.out.println(marin);
	
	}

}
