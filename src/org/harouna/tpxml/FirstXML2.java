package org.harouna.tpxml;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.harouna.tpxml.serie1Exercice2.*;

public class FirstXML2 {

	public static void main(String[] args) throws IOException, Throwable {
		// TODO Auto-generated method stub
			File xmlFile = new File("files/marin.xml");
			Document document = new XMLUtil().read(xmlFile);
			Marin marin = new XMLUtil().deserialize(document);
			
			System.out.println(marin);
			
			Element rootElement = DocumentFactory.getInstance().createElement("marin");
			
			rootElement.addAttribute("id", "12");
			rootElement.addElement("nom").addText("Surcouf");
			rootElement.addElement("prenom").addText("Robert");
			rootElement.addElement("age").addText("33");
			
			Document createdDocument = DocumentHelper.createDocument(rootElement);
			
			//Pretty Print
			FileWriter writer = new FileWriter(new File("files/created-marin.xml"));
			createdDocument.write(writer);
			
			writer.flush();
			writer.close();
			
			System.out.println("File created");

	}

}
