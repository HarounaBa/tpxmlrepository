package org.harouna.tpxml.sax;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class FirstExampleSAX {
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();
				
		File xmlFile = new File("files/marin.xml");
		
		DefaultHandler handler = new DefaultHandler(){
			public void startDocument(){
				System.out.println("Ouverture du document XML");
			}
			
			public void endDocument(){
				System.out.println("Fermeture du document XML");
			}
			
			public void startElement(String uri, String localName, String qName, Attributes attributes){
				System.out.println("uri       = " + uri);
				System.out.println("localName = " + localName);
				System.out.println("qName     = " + qName);
				
				int nombreAttributes = attributes.getLength();
				for(int i=0; i< nombreAttributes; i++){
					System.out.println("nom attribut       :" + attributes.getQName(i));
					System.out.println("valeur : " + attributes.getValue(i));
				}
			}
			
			public void endElement(String uri, String localName, String qName){
				System.out.println("Fermeture");

			}
			
			public void characters(char[] chars, int start, int length) {
				String string = new String(chars, start, length);
				System.out.println("contenu textuel = " + string);
			}
		};
		saxParser.parse(xmlFile, handler);
	}
}
