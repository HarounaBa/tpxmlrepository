package org.harouna.tpxml.serie1Exercice1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.harouna.tpxml.serie1Exercice2.*;
import org.harouna.tpxml.serie1Exercice2.Marin;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


public class XMLUtil {


	public Document serialize(org.harouna.tpxml.serie1Exercice1.Marin marin){
		
		
		Element rootElement = DocumentFactory.getInstance().createElement("marin");
		
		rootElement.addAttribute("id", String.valueOf(marin.getId()));
		rootElement.addElement("nom").addText(marin.getNom());
		rootElement.addElement("prenom").addText(marin.getPrenom());
		rootElement.addElement("age").addText(String.valueOf(marin.getAge()));
		
		Document createdDocument = DocumentHelper.createDocument(rootElement);
		return createdDocument;
	}
	
	public void write(Document document, File file) throws Throwable{
		
		file = new File("files/created-marin.xml");
		FileWriter writer = new FileWriter(file);
		document.write(writer);
		
		writer.flush();
		writer.close();
		
		System.out.println("File created");
	}

	



}
