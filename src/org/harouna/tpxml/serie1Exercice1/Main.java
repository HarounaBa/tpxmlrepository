package org.harouna.tpxml.serie1Exercice1;

import java.io.File;

import org.dom4j.Document;
import org.harouna.tpxml.serie1Exercice1.Marin;
import org.harouna.tpxml.serie1Exercice1.XMLUtil;

public class Main {

	public static void main(String[] args) throws Throwable {
		// TODO Auto-generated method stub
		Marin marin = new Marin(12,"ba","harouna",22);
		File xmlFile = new File("files/created-marin.xml");
		
		XMLUtil xml = new XMLUtil();
		Document document = xml.serialize(marin);
		
		xml.write(document, xmlFile);

		System.out.println(marin);
	}

}
