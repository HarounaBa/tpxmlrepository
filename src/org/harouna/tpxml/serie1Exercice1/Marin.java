package org.harouna.tpxml.serie1Exercice1;

import java.io.File;

import org.dom4j.Document;
import org.harouna.tpxml.serie1Exercice2.*;

public class Marin {
//private static long currendId=0;
	
	private long id;
	private String prenom;
	private String nom;
	private int age;
	
	public Marin() {
		//synchronized(Marin.class){
		//this.id = currendId++;
		//}
	}
	
	public Marin(String nom, String prenom) {
		this();
		//this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		//this.age = age;
	}
	
	public Marin(long id, String nom, String prenom, int age) {
		this();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		 this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Marin [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", age=" + age + "]";
	}
	
	

	
}
