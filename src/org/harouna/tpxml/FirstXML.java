package org.harouna.tpxml;

import java.io.File; //import� en faisant Ctrl-Shift-O sur File
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class FirstXML {

	public static void main(String[] args) throws DocumentException {
		// TODO Auto-generated method stub
			
		//Lecture du fichier XML marin.xml
		
		File xmlFile = new File("files/marin.xml");
		
		SAXReader saxReader = new SAXReader();
		
		//saxReader.read(xmlFile);
		Document document = saxReader.read(xmlFile); //Ctrl + 1
		Element rootElement = document.getRootElement(); //unique element  racine du xml (getRootElement())
		
		/*Affichage du nom de l'�l�ment racine*/
		String rootElementName = rootElement.getName(); //r�cupere le nom de l'element racine
		System.out.println("Nom de l'�l�ment racine  = " + rootElementName);
		
		//Attributs
		List<Attribute> attributes = rootElement.attributes();
		for(Attribute attribute : attributes){
			System.out.println("Nom attribut :" + attribute.getName());
			System.out.println("Valeur attribut : " + attribute.getStringValue());

		}
		
			//R�cuperation des sous �l�ments 
		List<Element> elements = rootElement.elements();
		for(Element subElement : elements){
			//Affichage des sous-elements
			System.out.println("Sous-�l�ment :" + subElement.getName());
			//Affichage valeurs
			System.out.println("String value :" + subElement.getStringValue()); //ou subElement.getText()
		}
	}

}
